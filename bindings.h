#ifndef BINDINGS_H
#define BINDINGS_H

#include "datatypes.h"
#include "departure.h"
#include "departurereply.h"
#include "departurerequest.h"
#include "journey.h"
#include "journeyreply.h"
#include "journeyrequest.h"
#include "kpublictransport_export.h"
#include "line.h"
#include "location.h"
#include "locationreply.h"
#include "locationrequest.h"
#include "manager.h"
#include "reply.h"

#endif
